﻿using System;

namespace variables_examples_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Which month were you born in?");

            var month = Console.ReadLine();

            Console.WriteLine($"You were born in {month}.");
        }
    }
}
